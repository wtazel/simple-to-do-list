<?php
	session_start();
	require_once("Tasklist.php");
	require_once("defines.php");
	
	
	$svr = "127.0.0.1";
	$usr = "root";
	$pass = "";
	$db = "todolist";
	
	/* Creates a Tasklist object with all the tasks from the list specified by $id */
	/* Returns the Tasklist object */
	function load($id) {
		$list_exists = false;
		global $svr;
		global $usr;
		global $pass;
		global $db;
		$conn = new mysqli($svr, $usr, $pass, $db);
		if($conn->connect_error) {
	
			return DB_ERR;
		}
		
		$res = $conn->query("SELECT * FROM lists WHERE list_id={$id} LIMIT 1");
		if($res) {
			if($res->num_rows > 0) {
				$list_exists = true;
			} else {
		
				return NO_LIST;
			}
		} else {
		
			return LIST_QUERY_ERR;
		}
		
		if($list_exists) {
			$tasklist = new Tasklist();
			$res = $conn->query("SELECT * FROM tasks WHERE list_id={$id} ORDER BY index_in_list ASC");
			if($res) {
				if($res->num_rows >0) {
					//Build a Tasklist object with Tasks
					while($task = $res->fetch_assoc()) {
						$t = new Task($task["task_id"], $task["title"], $task["important"], $task["status"]);
						$tasklist->insert(new Item($t));
						
					}
				}
				//Return the Tasklist object. It might contain no Tasks.
				return $tasklist;
			} else {
				
				return TASK_QUERY_ERR;
			}
		}
	}
	
	/* Creates a new list */
	/* Returns the list id */
	function create() {
		global $svr;
		global $usr;
		global $pass;
		global $db;
		$conn = new mysqli($svr, $usr, $pass, $db);
		if($conn->connect_error) {
			return DB_ERR;
		}
		
		$res = $conn->query("INSERT INTO lists VALUES()");
		return $conn->insert_id;
		
	}
	
	/* Creates a new task in the specified list */
	/* Returns the task id */
	function add($list_id, $title) {
		global $svr;
		global $usr;
		global $pass;
		global $db;
		$conn = new mysqli($svr, $usr, $pass, $db);
		if($conn->connect_error) {
			return DB_ERR;
		}
		$res = $conn->query("SELECT task_id FROM tasks WHERE list_id={$list_id}");
		$nt = $res->num_rows + 1;
		$res = $conn->query("INSERT INTO tasks(title, list_id, index_in_list) VALUES('{$title}', {$list_id}, {$nt})");
		if($conn->affected_rows <= 0) {
			return -1;
		}
		return $conn->insert_id;
	}
	
	/* Removes a task from the list. The list id must be specified. */
	/* Returns false in case of failure, or true otherwise. */
	function remove($task_id, $list_id) {
		global $svr;
		global $usr;
		global $pass;
		global $db;
		$conn = new mysqli($svr, $usr, $pass, $db);
		if($conn->connect_error) {
			return DB_ERR;
		}
		
		$res = $conn->query("DELETE FROM tasks WHERE task_id={$task_id} AND list_id={$list_id}");
		if($conn->affected_rows > 0) {
			return true;
		}
		return false;
	}
	
	/* Changes the status of the specified task with the new status. The list id must be specified */
	/* Returns false in case of failure, or true otherwise. */
	function changeStatus($status, $task_id, $list_id) {
		global $svr;
		global $usr;
		global $pass;
		global $db;
		$conn = new mysqli($svr, $usr, $pass, $db);
		if($conn->connect_error) {
			return DB_ERR;
		}
		
		$res = $conn->query("UPDATE tasks SET status={$status} WHERE task_id={$task_id} AND list_id={$list_id}");
		if($conn->affected_rows >0 ){
			return true;
		}
		return false;
	}	
	
	/* Used to print the status-box <div> element with the corresponding classes */
	/* Returns the <div> status-box element */
	function printStatus($status) {
		switch($status) {
			case 1:
				$sc = "status-finished";
				break;
			case 2:
				$sc = "status-failed";
				break;
			default:
				$sc = "";
		}
		
		return '<div class="status-box '.$sc.'"></div>';
	}
	
	/* Refreshes the task order from a list in the database. The list id must be specified. */
	/* Returns false in case of failure, or true otherwise. */
	function refreshOrder($tidarray, $lid) {
		if(count($tidarray) < 2) {
			return false;
		}
		
		global $svr;
		global $usr;
		global $pass;
		global $db;
		$conn = new mysqli($svr, $usr, $pass, $db);
		if($conn->connect_error) {
			return DB_ERR;
		}
		
		$conditions = "";
		$task_ids = "";
		$i = 1;
		foreach($tidarray as $tid) {
			$conditions .= "when task_id = {$tid} then {$i} ";
			$task_ids .= "{$tid},";
			$i++;
		}
		$task_ids = rtrim($task_ids, ',');
		$res = $conn->query("
			UPDATE tasks
			SET index_in_list = ( case {$conditions}
									end)
			WHERE task_id in ({$task_ids}) AND list_id = {$lid}
			");
		
		if($conn->affected_rows > 0) {
			return true;
		}
		return false;
	}
	
	/* Checks if the specified code is an error. */
	/* Returns true if the code is an error, or false otherwise */
	function errchk($r) {
		if(is_int($r)) {
			switch($r) {
				case DB_ERR:
					return true;
				case NO_LIST:
					return true;
				case LIST_QUERY_ERR:
					return true;
				case TASK_QUERY_ERR:
					return true;
				default:
					return false;
			}
		} else {
			return false;
		}
	}
	
	/* This part handles the AJAX requests from taskman.js */
	if(isset($_POST['action'])) {
		$action = htmlspecialchars($_POST['action']);
		if($action == "add") {
			if(isset($_POST['tt']) && isset($_SESSION['llid'])) {
				$tt = htmlspecialchars($_POST['tt']);
				if(strlen($tt) > 0) {
					$task_id = add($_SESSION['llid'], $tt);
					echo '
						<div class="task" data-id="'.$task_id.'">
							<div class="status-box"></div>
							<div class="title">'.$tt.'</div>
							<div class="date"></div>
							<button class="options-task-btn hidden-btn"></button>
							<div class="options-menu">
								<ul>
									<li>'.BTN_PRIORITY.'
										<ul>
											<li class="mark-important-btn">'.BTN_MARK_IMPORTANT.'</li>
											<li class="send-top-btn">'.BTN_SEND_TO_TOP.'</li>
											<li class="send-bottom-btn">'.BTN_SEND_TO_BOTTOM.'</li>
										</ul>
									</li>
									<li class="remove-task-btn">'.BTN_REMOVE.'</li>
								</ul>
							</div>
							<div class="importance"></div>
						</div>';
				}
			}
			
		} else if($action == "remove") {
			if(isset($_POST['tid']) && isset($_SESSION['llid'])) {
				$tid = htmlspecialchars($_POST['tid']);
				$res = remove($tid, $_SESSION['llid']);
				if($res) {
					echo "done";
				}
			}
		} else if($action == "c_stat") {
			if(isset($_POST['stat']) && isset($_SESSION['llid']) && isset($_POST['tid'])) {
				$tid = htmlspecialchars($_POST['tid']);
				$stat = htmlspecialchars($_POST['stat']);
				if(($stat >= 0) && ($stat <= 2))
				{
					$res = changeStatus($stat, $tid, $_SESSION['llid']);
					if($res) {
						echo "1";
					}
				}
				
			}
		} else if($action == "refresh") {
			if(isset($_POST['tids']) && isset($_SESSION['llid'])) {
				$tids = $_POST['tids'];
				foreach($tids as &$tid) {
					$tid = htmlspecialchars($tid);
				}
				$res = refreshOrder($tids, $_SESSION['llid']);
				if($res) {
					echo "1";
				}
			}
		}
	}
?>