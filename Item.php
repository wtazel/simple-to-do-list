<?php
	require_once("Task.php");
	
	class Item
	{
		protected $_key = null;
		protected $_next = null;
	 
		public function __construct(Task $key)
		{
			$this->_key = $key;
		}
	 
		public function setNext(&$next) { $this->_next = $next; }
		public function &getNext() { return $this->_next; }
	 
		public function setKey(Task $key) { $this->_key = $key; }
		public function getKey() { return $this->_key; }
	}

?>