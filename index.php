<?php
	require_once("taskman.php");
?>

<?php
	if(isset($_GET["id"])) {
		$id = $_GET["id"];
		$id = htmlspecialchars($id);
		$id = intval($id);
		//echo $id;
		if(is_int($id)) {
			//Load list
			$res = load($id);
			if(errchk($res) == true) {
				if($res == NO_LIST) {
					die("This list does not exist");
				} else {
					die("An error has occured (#{$res}). Please come back later.");
				}
			}
			$_SESSION['llid'] = $id;
			
		} else {
			die("An error has occured. Please come back later.");
		}
	} else {
		//Create list
		$new_list_id = create();
		if($new_list_id > 0) {
			header("Location: index.php?id={$new_list_id}");
			exit;
		} else {
			die("An error has occured. Please come back later.");
		}
	}
	

?>

<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="jquery-ui.css">
		<link rel="stylesheet" type="text/css" href="style.css">
		<script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
		<script src="taskman.js"></script>
		<title>Simple To-Do List</title>
		
	</head>
	<body>
		<div id="container">
			<!--<div id="option-btn"></div>-->
			<input type="text" id="add-task" placeholder="Enter new task here..." autofocus/>
			<button class="btn" id="add-task-btn"></button>
			
			<div id="task-list-container">
				<div id="task-list">
					<?php
						if(!$res->isEmpty()) {
							for($i=0; $i<$res->size(); $i++) {
								echo '
									<div class="task" data-id="'.$res->find($i)->getTaskId().'">
										'.printStatus($res->find($i)->getStatus()).'
										<div class="title">'.$res->find($i)->getTitle().'</div>
										<div class="date"></div>
										<button class="options-task-btn hidden-btn"></button>
										<div class="options-menu">
											<ul>
												<li>'.BTN_PRIORITY.'
													<ul>
														<li class="mark-important-btn">'.BTN_MARK_IMPORTANT.'</li>
														<li class="send-top-btn">'.BTN_SEND_TO_TOP.'</li>
														<li class="send-bottom-btn">'.BTN_SEND_TO_BOTTOM.'</li>
													</ul>
												</li>
												<li class="remove-task-btn">'.BTN_REMOVE.'</li>
											</ul>
										</div>
										<div class="importance"></div>
									</div>';
							}
						}
					?>
				</div>
			</div>
		</div>
		
	</body>
</html>