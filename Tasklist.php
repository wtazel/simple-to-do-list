<?php

	require_once("Item.php");
	
	class Tasklist {
		protected $_head = null;
		protected $_tail = null;
		protected $_size = 0;
		protected $_cur = null;
	 
		public function insert(Item $item)
		{
			if ($this->_head == null) {
				$this->_head = $item;
				$this->_tail = $item;
				$this->_cur = $item;
				$this->_size++;
				return $this->_size;;
			}
	 
			$this->_tail->setNext($item);
			$this->_tail = $item;
			$this->_cur = $this->_tail;
			$this->_size++;
			return $this->_size;
		}
		
		public function findItem($index)
		{
			if(is_int($index) === FALSE) {
				trigger_error('find() expected argument 1 to be int.', E_USER_WARNING);
				return;
			}
			if($index > $this->_size-1) {
				trigger_error('Target index exceeds the number of items in the list.', E_USER_WARNING);
				return;
			}
			$this->_cur = $this->_head;
			$cur_index = 0;
			while($cur_index != $index) {
				$this->_cur = $this->_cur->getNext();
				$cur_index++;
			}
			return $this->_cur;
			
		}
		
		/**
		* Returns the task located at the specified index number in the list.
		*/
		public function find($index)
		{
			return $this->findItem($index)->getKey();
		}
		
		public function isEmpty()
		{
			if($this->_size>0) return false;
			return true;
		}
		
		public function size()
		{
			return $this->_size;
		}
		
		public function emptyList()
		{
			$this->_head = null;
			$this->_tail = null;
		}
	}


?>