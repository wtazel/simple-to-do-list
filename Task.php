<?php

	class Task {
		
		private $id = 0;
		private $title = "";
		private $status = 0;
		private $important = false;
		
		public function __construct($task_id, $title, $important = false, $status=0) {
			$this->id = $task_id;
			$this->title = $title;
			if($important === true) {
				$this->important = $important;
			} else {
				$this->important = $important;
			}
			if(($status >= 0) && ($status <=2))
				$this->status = $status;
		}
		
		public function setStatus($status) {
			if(($status >= 0) && ($status <=2))
				$this->status = $status;
		}
		
		public function setTitle($title) {
			$this->title = $title;
		}
		
		public function setImportance($imp) {
			if($imp === true) {
				$this->important = $imp;
			} else {
				$this->important = $imp;
			}
		}
		
		public function getStatus() {
			return $this->status;
		}
		
		public function getTitle() {
			return $this->title;
		}
		
		public function isImportant() {
			return $this->important;
		}
		
		public function getTaskId() {
			return $this->id;
		}
		
	}

?>