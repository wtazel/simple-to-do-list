<?php
	//Errors
	define("DB_ERR", 101);
	define("NO_LIST", 102);
	define("LIST_QUERY_ERR", 103);
	define("TASK_QUERY_ERR", 104);
	
	define("BTN_PRIORITY", "Priority <span style=\"float: right; position:relative; top: -1px;\">&#9654;</span>");
	define("BTN_MARK_IMPORTANT", "Mark");
	define("BTN_SEND_TO_TOP", "First");
	define("BTN_SEND_TO_BOTTOM", "Last");
	define("BTN_REMOVE", "Remove");

?>