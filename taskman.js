$(document).ready(function() {
	var add_task = $('#add-task');
	var status_box = $('.status-box');
	var task_list = $('#task-list');
	
	
	
	$('#add-task-btn').click(function() {
		console.log(add_task.val());
		if(add_task.val().length > 0) {
			addTask(add_task.val());
			add_task.val('');
		}
	});
	
	add_task.keypress(function(e) {
		 var code = e.keyCode || e.which;
		 //'Enter' key pressed
		 if(code == 13) { 
			$('#add-task-btn').trigger("click");
		 }
	});
	var ind;
	task_list.sortable({
		
		start: function(event, ui) {
			ind = ui.item.index();
		},
		stop: function(event, ui) {
			if(ind != ui.item.index()) {
				/* Refresh list in database with the new order */
				refreshDbList();
			}
		}
	});
	task_list.disableSelection();
	
	task_list.on('click', '.task .status-box', function() {
		toggleStatus($(this));
	});
	task_list.on('click', '.task .remove-task-btn', function(e) {
		removeTask($(this).closest('.task'));
		
	});
	
	$('.remove-task-btn').click(function(e) {
		removeTask($(this).closest('.task'));
		
	});
	

	task_list.on('click', '.task .mark-important-btn', function() {
		$(this).closest('.task').find('.importance').toggleClass('is-imp');
	});
	$('.mark-important-btn').click(function(e) {
		$(this).closest('.task').find('.importance').toggleClass('is-imp');
	});
	
	task_list.on('click', '.task .send-top-btn', function() {
		sendToTop($(this).closest('.task'));
	});
	$('.send-top-btn').click(function(e) {
		sendToTop($(this).closest('.task'));
	});
	
	task_list.on('click', '.task .send-bottom-btn', function() {
		sendToBottom($(this).closest('.task'));
	});
	$('.send-bottom-btn').click(function() {
		sendToBottom($(this).closest('.task'));
	});
	
	$(document).click(function() {
		$('.options-menu').css("display","none");
	});
	
	
	task_list.on('click', '.task .options-task-btn', function(e) {
		e.stopPropagation();
		$('.options-menu').css("display","none");
		var op_menu = $(this).siblings('.options-menu');
		if(op_menu.css("display") == "none")
			op_menu.css("display","inline-block");
		else if(op_menu.css("display") == "inline-block")
			op_menu.css("display","none");
	});
	
	/*
	task_list.on('click', '.task .remove-date-btn', function() {
		$(this).closest('.task').find('.date').empty();
		$(this).closest('.task').find('.date').hide();
	});
	task_list.on('click', '.task .set-date-btn', function() {
		$(this).closest('.task').find('.date').html("Today");
		$(this).closest('.task').find('.date').show();
	});
	*/
	
});


function addTask(title) {
	var request = $.ajax({
	  url: "taskman.php",
	  method: "POST",
	  data: { action : "add", tt : title },
	  dataType: "html"
	});
	 
	request.done(function( msg ) {
		if(msg.length > 0) {
			console.log(msg);
			new_task = $(msg);
			$('#task-list').append(new_task);
		}
	});
	 
	request.fail(function( jqXHR, textStatus ) {
		alert( "Request failed: " + textStatus );
	});

/*
	new_task = $('<div class="task" data-id=""></div>');
	new_task_status = $('<div class="status-box"></div>');
	new_task_title = $('<div class="title"></div>');
	new_task_date = $('<div class="date"></div>');
	new_task_options = $('<button class="options-task-btn hidden-btn"></button>');
	new_task_op_menu = $('<div class="options-menu"><ul><li>Priority<ul><li class="mark-important-btn">Mark as Important</li><li class="send-top-btn">Place on top</li><li class="send-bottom-btn">Place at bottom</li></ul></li><li class="remove-task-btn">Remove</li></ul></div>');
	new_task_important = $('<div class="importance"></div>');
	
	new_task_rbutton = $('<button class="btn remove-task-btn hidden-btn"></button>');
	new_task_starbutton = $('<button class="btn star-task-btn hidden-btn"></button>');
	new_task_datebutton = $('<button class="btn date-task-btn hidden-btn"></button>');
	new_task_priority = $('<div class="priority p-normal"></div>');
	new_task_separator = $('<span class="separator">&bull;</span>');
	new_task_op_menu = $('<div class="options-menu"><ul><li>Date<ul><li class="set-date-btn">Set date</li><li class="remove-date-btn">Remove date</li></ul></li><li>Priority<ul><li class="mark-important-btn">Mark as Important</li><li class="send-top-btn">Place on top</li><li class="send-bottom-btn">Place at bottom</li></ul></li><li class="remove-task-btn">Remove</li></ul></div>');
	*/
	
	
	
	/*
	new_task_title.html(title);
	new_task.append(new_task_status);
	new_task.append(new_task_title);
	new_task.append(new_task_date);
	new_task.append(new_task_important);
	new_task.append(new_task_options);
	new_task.append(new_task_op_menu);
	*/

}
function sendToTop(task) {
	$('#task-list').prepend(task);
	refreshDbList();
}

function sendToBottom(task) {
	$('#task-list').append(task);
	refreshDbList();
}

function removeTask(task) {
	
	var request = $.ajax({
	  url: "taskman.php",
	  method: "POST",
	  data: { action : "remove", tid : task.data("id") },
	  dataType: "html"
	});
	 
	request.done(function( msg ) {
		if(msg == "done") {
			task.remove();
		}
	});
	 
	request.fail(function( jqXHR, textStatus ) {
		alert( "Request failed: " + textStatus );
	});
	refreshDbList();
}

/*
function toggleTaskPriority(task) {
	var p = task.find('.priority');
	if(p.hasClass('p-normal')) {
		p.removeClass('p-normal');
		p.addClass('p-high');
	} else if(p.hasClass('p-high')) {
		p.removeClass('p-high');
		p.addClass('p-low');
	} else if(p.hasClass('p-low')) {
		p.removeClass('p-low');
		p.addClass('p-normal');
		
	}
}
*/


function toggleStatus(status_box)
{
	var s;
	var t = status_box.closest('.task').find('.title');
	if(status_box.hasClass('status-finished')) {
		status_box.removeClass('status-finished');
		status_box.addClass('status-failed');
		t.css("text-decoration", "line-through");
		t.css("font-style", "italic");
		t.css("color", "black");
		s = 2;
	} else if(status_box.hasClass('status-failed')) {
		status_box.removeClass('status-failed');
		t.css("text-decoration", "none");
		t.css("font-style", "normal");
		t.css("color", "black");
		s = 0;
	} else {
		status_box.addClass('status-finished');
		t.css("font-style", "italic");
		t.css("color", "gray");
		s = 1;
	}
	
	var request = $.ajax({
	  url: "taskman.php",
	  method: "POST",
	  data: { action : "c_stat", stat : s, tid : status_box.closest('.task').data("id") },
	  dataType: "html"
	});
	 
	request.done(function( msg ) {
		if(msg == "1") {
			console.log("The action has been completed successfully.");
		} else {
			console.log("Failed to complete the action.");
		}
	});
	 
	request.fail(function( jqXHR, textStatus ) {
		alert( "Request failed: " + textStatus );
	});
}

function refreshDbList() {
	t = [];
	$('.task').each(function(index) {
		t[index] = $(this).data('id');
	});
	
	var request = $.ajax({
	  url: "taskman.php",
	  method: "POST",
	  data: { action : "refresh", tids : t },
	  dataType: "html"
	});
	 
	request.done(function( msg ) {
		if(msg == "1") {
			console.log("The action has been completed successfully.");
		} else {
			console.log("Failed to complete the action.");
		}
	});
	 
	request.fail(function( jqXHR, textStatus ) {
		alert( "Request failed: " + textStatus );
	});
}